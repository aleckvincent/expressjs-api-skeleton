# ExpressJS API skeleton

This project is a skeleton for setup easily an ExpressJS project

### Pre-installed dependancies
- mangoose
- expressjs
- body-parser


### Installation
Please copy ``docker-compose-dist.yml`` and rename it as ``docker-compose.yml``
>Change ``working_dir`` and ``volumes`` properties

>In ``./config/mongodb.js`` adapt IP address and port to you environment

After that, run :  
```bash 
$ docker-compose up -d 
```
### Tips
To see your running containers
```bash
$ docker ps
```

To get your container's IP 
```bash
$ docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <container_id>
```