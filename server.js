const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dbConfig = require('./config/mongodb.js');


// body parsing
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// routing
require('./app/routes/routing.js')(app);

// database (mongoose)
mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});



// server
const port = process.env.PORT || '3000';
app.listen(port, () => {
  console.log('Server listening on port ' + port);
});

